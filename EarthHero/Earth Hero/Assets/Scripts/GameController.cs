﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void DifficultyChangeAction(Difficulty difficulty);

public enum Difficulty
{
    Easy,
    Middle,
    Hard
}

public class GameController : MonoBehaviour
{
  
    public delegate void PointsAddAction(int points);
    public delegate void GameOverAction(string endGamePhrase, bool showRestartButton);
 

    public static event PointsAddAction AddPointsEvent;
    public static event GameOverAction GameOverEvent;
   

    private const float DistanceAboveScreen = 2f;
    private const int PointsForEnemy = 10;
    private const int PointsForMiniBoss = 20;
    

    [SerializeField]
    private int _blocksOfEnemies = 3;
    [SerializeField]
    private int _enemiesInRow = 4;
    [SerializeField]
    private int _enemiesInColumn = 3;
    [SerializeField]
    private GameObject _enemy = null;
    [SerializeField]
    private GameObject _miniBoss = null;
    [SerializeField]
    private GameObject _playerPrefab = null;
    [SerializeField]
    private float _distanceBetweenEnemies = default;
    [SerializeField]
    private float _distanceBetweenRows = default;
    [SerializeField]
    private float _distanceBetweenBlocks = default;
    [SerializeField]
    private int _maxLevel = default;
    [SerializeField]
    private GameObject _powerUp = null;
    [SerializeField]
    private AudioSource _musicAudio = null;
    [SerializeField]
    private AudioSource _sfxAudio = null;
 
    private List<GameObject> _enemies = new List<GameObject>();
    private bool isWaveCreating=true;
    private int _level=1;
    private Vector3 _maxPoint;
    private float _xDestinationPoint;
    private float _yDestinationPoint;
    private Vector3 _instancePoint;
    private Vector3 _miniBossDestination;
    private GameObject _playerInstance;
    private int _difficultyModifier = 2;


    

    void Awake()
    {
        FindStartPoint();
        
        UIController.StartGameEvent += StartNewGame;
        EnemyController.EnemyDestroyedEvent += DeleteFromList;
        UIController.RestartGameEvent += RestartGame;
        UIController.StopGameEvent += StopGame;
        DifficultyChoosing.DifficultyChangeEvent += DifficultySetup;

        DifficultySetup((Difficulty)PlayerPrefs.GetInt("Difficulty", 0));
    }


    private void Update()
    {
        if ((_enemies.Count <= 0) && !isWaveCreating && _level<=_maxLevel)
        {
            WaveCreate();
        }
    }


    private void StartNewGame()
    {
        _level = 1;
        PlayerCreation();
        WaveCreate();
    }


    private void PlayerCreation()
    {
        var player = Instantiate(_playerPrefab, new Vector3(0f, (-_maxPoint.y + _playerPrefab.GetComponent<BoxCollider2D>().size.y * 0.5f), 0f), Quaternion.identity);
        player.GetComponent<PlayerController>().Boundaries = new Boundaries(_maxPoint);
     
    }

    private void WaveCreate()
    {
        isWaveCreating = true;
        for (int k = 0; k < _blocksOfEnemies; k++)
        {
            for (int j = 0; j < _enemiesInColumn; j++)
            {
                for (int i = 0; i < (_enemiesInRow+_difficultyModifier); i++)
                {
                    _enemies.Add(Instantiate(_enemy, _instancePoint, _enemy.transform.rotation));
                    GameObject enemy =
                    _enemies[_enemies.Count - 1];
                    SetEnemyStatement(
                        new Vector3(_xDestinationPoint + (_distanceBetweenEnemies * i), _yDestinationPoint + (j * _distanceBetweenRows), 0f),
                        _level, 
                        enemy
                        );
                }
            }
            _xDestinationPoint += _distanceBetweenBlocks;
        }
        _xDestinationPoint -= _distanceBetweenBlocks * (_blocksOfEnemies);

        _enemies.Add(Instantiate(_miniBoss, _instancePoint, _enemy.transform.rotation));
        GameObject miniBoss = _enemies[_enemies.Count - 1];
        _miniBossDestination.Set(0f, _maxPoint.y - (_miniBoss.GetComponentInChildren<BoxCollider2D>().size.y * 0.5f), 0f);
        SetEnemyStatement(_miniBossDestination, _level, miniBoss);
       
        miniBoss.GetComponentInChildren<MiniBossController>().Player = _playerPrefab.transform;
        miniBoss.GetComponentInChildren<MiniBossController>().LevelModifier = _level;

        isWaveCreating = false;
        _level++;
    }


    private void SetEnemyStatement(Vector2 destination, int level, GameObject go)
    {
        go.GetComponent<EnemyContainerMovement>().PointB = destination;
        go.GetComponentInChildren<EnemyController>().LevelModifier = level;        
    }


    private void DeleteFromList(GameObject elementToDelete, Vector2 enemyPosition)
    {
        if (AddPointsEvent != null)
        {
            if (elementToDelete.tag == "Enemy")
            {
                AddPointsEvent(PointsForEnemy);
            }
            else
            {
                AddPointsEvent(PointsForMiniBoss);
            }
        }
            
        _enemies.Remove(elementToDelete);
       
        if (_enemies.Count == (_blocksOfEnemies * _enemiesInRow * _enemiesInColumn)*0.5f )
        {
            CreatePowerUp(enemyPosition);
        }

        if ((_level > _maxLevel) && (_enemies.Count ==0))
        {
            if (GameOverEvent != null)
                GameOverEvent("You Win", true);
        }
    }


    private void FindStartPoint()
    {
        var camera = GetComponent<Camera>();
        _maxPoint = camera.ViewportToWorldPoint(new Vector3(1f, 1f, camera.nearClipPlane));

        float xCenter = _maxPoint.x - (_maxPoint.x / _blocksOfEnemies);

        _xDestinationPoint = xCenter - (0.5f * _enemy.GetComponentInChildren<BoxCollider2D>().size.x) * (_enemiesInRow - 1);
        _yDestinationPoint = 0f - (0.5f * _enemy.GetComponentInChildren<BoxCollider2D>().size.y) * (_enemiesInColumn - 1);
        _instancePoint.Set(0f, _maxPoint.y+DistanceAboveScreen, 0f);
    }


    private void CreatePowerUp(Vector2 position)
    {
      var powerUp =  Instantiate(_powerUp, position, Quaternion.identity);
    }


    private void VolumeChange(float musicVolume, float sfxVolume)
    {
        _musicAudio.volume = musicVolume;
        _sfxAudio.volume = sfxVolume;
    }


    private  void SFXPlay(AudioClip clipToPlay)
    {
        _sfxAudio.clip = clipToPlay;
        _sfxAudio.Play();
    }


    private void RestartGame()
    {
        DeleteAllObjects(true);
    }


    private void StopGame()
    {
        DeleteAllObjects(false);
        isWaveCreating = true;
    }


    private void DeleteAllObjects(bool isRestart)
    {
        for (int i = (_enemies.Count - 1); i >= 0; i--)
        {
            Destroy(_enemies[i].gameObject);
            _enemies.RemoveAt(i);
        }
        var player = FindObjectOfType<PlayerController>();
        if ( player!= null)
        {
            Destroy(player.gameObject);
        }
        if(isRestart)
        StartNewGame();
    }

    private void DifficultySetup(Difficulty difficulty)
    {
        if (difficulty == Difficulty.Easy)
        {
            _difficultyModifier = 0;
        }
        else
        {
            _difficultyModifier = 2;
        }

        
    }
}
