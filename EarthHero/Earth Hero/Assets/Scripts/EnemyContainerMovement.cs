﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyContainerMovement : MonoBehaviour
{
    
    private const float MoveDownDistance = 1f;

  
    [SerializeField]
    private float _duration=3f;
    [SerializeField]
    private float _moveDownDelay=15f;
  
    private Vector3 _pointA;
    private float _timer = 0;
    private float _moveDownTimer=0f;
    private Vector3 _pointB;
    

    public Vector3 PointB
    {
        get => _pointB; 
        set => _pointB = value; 
    }

   

    void Start()
    {
        _pointA = transform.position;
    }

  
    void Update()
    {
        MoveObjectByLerp();

        _moveDownTimer += Time.deltaTime;
        if (_moveDownDelay < _moveDownTimer)
        {
            _pointA = transform.position;
            _pointB = new Vector2(transform.position.x, (transform.position.y - MoveDownDistance));
            _timer = 0f;
            _moveDownTimer = 0f;
        }
    }


    private void MoveObjectByLerp()
    {
        if (_timer >= _duration)
            return;

        _timer += Time.deltaTime;
        _timer = Mathf.Clamp(_timer, 0, _duration);
        var timer01 = _timer / _duration;
        transform.position = Vector3.Lerp(_pointA, _pointB, timer01);
    }

    
}
