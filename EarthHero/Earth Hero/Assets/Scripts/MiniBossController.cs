﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniBossController : MonoBehaviour
{
 
    [HideInInspector]
    public float LevelModifier;
    [HideInInspector]
    public Transform Player;

    [SerializeField]
    private Transform _bulletSpawn = null;
    [SerializeField]
    private GameObject _bullet = null;
    [SerializeField]
    private float _speed = default;
    [SerializeField]
    private float _shootingPeriod = default;
    [SerializeField]
    private AudioSource _shootingAudioSource = null;

    private float _timer;
    private float _difficultyModifier = 1;

    private void Awake()
    {
        DifficultyChoosing.DifficultyChangeEvent += DifficultySetup;
        DifficultySetup((Difficulty)PlayerPrefs.GetInt("Difficulty", 0));
    }

    void Update()
    {
        _timer += Time.deltaTime;
        if (((_shootingPeriod/(LevelModifier*_difficultyModifier))<_timer) && (Player !=null))
        {
            Shot();
            _timer = 0f;
        }
    }

    private void Shot()
    {

        _shootingAudioSource.Play();
        var direction = (Player.position - _bulletSpawn.position);
        float xCos = Vector2.Dot(direction, Vector2.right);
        float ySin = Vector2.Dot(direction, Vector2.up);

        GameObject bulletInstance = Instantiate(_bullet, _bulletSpawn.position, Quaternion.identity);
        bulletInstance.GetComponent<Rigidbody2D>().velocity = new Vector2(_speed * xCos, _speed * ySin);
    }

    private void DifficultySetup(Difficulty difficulty)
    {
        if (difficulty == Difficulty.Hard)
        {
            _difficultyModifier = 2f;
           
        }

        else
        {
            _difficultyModifier = 1f;
        }

        
    }
}
