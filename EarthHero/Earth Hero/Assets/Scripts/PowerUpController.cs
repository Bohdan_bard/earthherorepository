﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PowerUpType
{
    AdditionalLife,
    MassFire
}


public class PowerUpController : MonoBehaviour
{

    private const int DestroyerLayer = 15;

    [SerializeField]
    private AudioSource _powerupSource = null;

    private PowerUpType _powerUpType;

    public PowerUpType PowerUpType
    {
        get => _powerUpType;       
        set => _powerUpType = value;      
    }

    private void Awake()
    {
        float i = Random.Range(0, 2);
       
        _powerUpType =  (PowerUpType)i;       
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == DestroyerLayer)
        {
            DestroyGameObject();
            return;
        }
        GetComponent<BoxCollider2D>().enabled = false;
        GetComponent<SpriteRenderer>().enabled = false;

        _powerupSource.Play();
       
        Invoke("DestroyGameObject", 3f);
    }

    private void DestroyGameObject()
    {
        Destroy(gameObject);
    }
}