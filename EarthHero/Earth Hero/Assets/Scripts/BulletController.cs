﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    private  const int EnemyBulletsLayer = 8;

    [SerializeField]
    private GameObject _explosion = null;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == EnemyBulletsLayer)
        {
            Instantiate(_explosion, transform.position, transform.rotation);
        }
        Destroy(gameObject);
    }

}
