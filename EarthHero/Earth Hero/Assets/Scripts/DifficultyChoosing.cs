﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DifficultyChoosing : MonoBehaviour
{
    public static event DifficultyChangeAction DifficultyChangeEvent;

  
    private Dropdown _difficultyDropdown;

    private void Awake()
    {
        _difficultyDropdown = GetComponent<Dropdown>();
    }

    private void Start()
    {
       
        _difficultyDropdown.value = (PlayerPrefs.GetInt("Difficulty", 0));

        _difficultyDropdown.onValueChanged.AddListener(delegate { ChangeDifficulty(_difficultyDropdown); });
        
    }

    private void ChangeDifficulty(Dropdown difficulty)
    {
      

        PlayerPrefs.SetInt("Difficulty", difficulty.value);

        DifficultyChangeEvent((Difficulty)difficulty.value);
    }
   
}
