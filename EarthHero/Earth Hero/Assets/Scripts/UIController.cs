﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;



public class UIController : MonoBehaviour
{
    public delegate void StartGameAction();
    


    public static event StartGameAction StartGameEvent;
    public static event StartGameAction RestartGameEvent;
    public static event StartGameAction StopGameEvent;
 
    

    

    [SerializeField]
    private Text _livesText = null;
    [SerializeField]
    private Image _massFireIcon = null;
    [SerializeField]
    private Slider _reloadSlider = null;
    [SerializeField]
    private Text _pointsText = null;
    [SerializeField]
    private Text _gameOverText = null;
    [SerializeField]
    private Button _startButton = null;
    [SerializeField]
    private GameObject _hudPanel = null;
    [SerializeField]
    private Slider _musicVolumeSlider = null;
    [SerializeField]
    private Slider _sfxVolumeSlider = null;
    [SerializeField]
    private Button _saveVolumeButton = null;
    [SerializeField]
    private Button _settingsButtonMainMenu = null;
    [SerializeField]
    private Button _settingsButtonPauseMenu = null;
    [SerializeField]
    private Button _exitButton = null;
    [SerializeField]
    private Button _pauseButton = null;
    [SerializeField]
    private GameObject _pauseMenu = null;
    [SerializeField]
    private Button _resumeButton = null;
    [SerializeField]
    private Button _exitToMainMenuButton = null;
    [SerializeField]
    private GameObject _mainMenu = null;
    [SerializeField]
    private Button _restartButton = null;
    [SerializeField]
    private AudioSource _audioSource = null;
    [SerializeField]
    private AudioMixer _musicMixer = null;
    [SerializeField]
    private AudioMixer _sfxMixer = null;
    [SerializeField]
    private Text _finalScoreText = null;
    [SerializeField]
    private Button _backToMainMenuButton = null;
    [SerializeField]
    private Button _helpButton = null;
    [SerializeField]
    private GameObject _helpMenu = null;
   

    private int _points = 0;
    private GameObject _settingsButtonParent;


    void Start()
    {

        SetStartSoundVolume();

        GameController.AddPointsEvent += AddPoints;
        GameController.GameOverEvent += GameOverTextShow;
        PlayerController.LifeChangeEvent += ShowLives;
        PlayerController.MassFireToggleEvent += MassFireOn;
        PlayerController.ReloadEvent += Reload;

        _settingsButtonMainMenu.onClick.AddListener(delegate { SettingsMenuActivation(_settingsButtonMainMenu.transform.parent.gameObject); });
        _settingsButtonPauseMenu.onClick.AddListener(delegate { SettingsMenuActivation(_settingsButtonPauseMenu.transform.parent.gameObject); });

        _startButton.onClick.AddListener(StartGame);
        _saveVolumeButton.onClick.AddListener(SaveVolume);
        _exitButton.onClick.AddListener(ExitGame);
        _pauseButton.onClick.AddListener(PauseGame);
        _resumeButton.onClick.AddListener(ResumeGame);
        _restartButton.onClick.AddListener(RestartGame);
        _exitToMainMenuButton.onClick.AddListener(ExitToMainMenu);
        _helpButton.onClick.AddListener(ActivateOrDiactivateHelpMenu);
        _backToMainMenuButton.onClick.AddListener(ActivateOrDiactivateHelpMenu);
    }



    private void AddPoints(int points)
    {
        _points += points;
        _pointsText.text = _points.ToString();
    }


    private void GameOverTextShow(string gameOverText, bool restartButtonShow)
    {
        _gameOverText.text = gameOverText;
        _finalScoreText.text = _points.ToString();
        _restartButton.gameObject.SetActive(restartButtonShow);
    }


    private void ShowLives(int lives)
    {
        if (lives <= 0)
        {
            
            GameOverTextShow("You died!", true);
        }
        _livesText.text = $"x{lives}";
    }


    private void MassFireOn(bool isActive)
    {
        if (isActive)
        {
            var baseColor = _massFireIcon.color;
            _massFireIcon.color = new Color(baseColor.r, baseColor.g, baseColor.b, 1f);
        }
        else {
            var baseColor = _massFireIcon.color;
            _massFireIcon.color = new Color(baseColor.r, baseColor.g, baseColor.b, 0f);
        }
    }


    private void Reload(float delay)
    {
        float timer = 0f;
        _reloadSlider.value = 0f;
        StartCoroutine(Reloading(timer, delay));
    }


    private IEnumerator Reloading(float timer, float delay)
    {
        while (_reloadSlider.value != 1f)
        {
            timer += Time.deltaTime;
            var progress = timer / delay;
            _reloadSlider.value = progress;
            yield return null;
        }
        _audioSource.Play();
    }


    private void StartGame()
    {
        if (StartGameEvent != null)
            StartGameEvent();
        _startButton.transform.parent.gameObject.SetActive(false);
        _hudPanel.SetActive(true);
        AddPoints(-_points);
    }


    private void SettingsMenuActivation(GameObject parent)
    {
        _settingsButtonParent = parent;
        _saveVolumeButton.transform.parent.gameObject.SetActive(true);
        _settingsButtonParent.SetActive(false);
    }


    private void SaveVolume()
    {
        var musicVolume = _musicVolumeSlider.value;
        var sfxVolume = _sfxVolumeSlider.value;
        _musicMixer.SetFloat("musicVolume", musicVolume);
        _sfxMixer.SetFloat("sfxVolume", sfxVolume);
        PlayerPrefs.SetFloat("MusicVolume", musicVolume);
        PlayerPrefs.SetFloat("SFXVolume", sfxVolume);
        _saveVolumeButton.transform.parent.gameObject.SetActive(false);
        _settingsButtonParent.SetActive(true);
    }


    private void ExitGame()
    {
        Application.Quit();
    }


    private void PauseGame()
    {
        Time.timeScale = 0f;
        _hudPanel.SetActive(false);
        _pauseMenu.SetActive(true);
    }


    private void ResumeGame()
    {
        _hudPanel.SetActive(true);
        _pauseMenu.SetActive(false);
        Time.timeScale = 1f;
    }


    private void ExitToMainMenu()
    {
        _pauseMenu.SetActive(false);
        _mainMenu.SetActive(true);
        Time.timeScale = 1f;
        StopGameEvent();
    }


    private void RestartGame()
    {
        _restartButton.gameObject.SetActive(false);
        GameOverTextShow(string.Empty, false);
        AddPoints(-_points);
        _finalScoreText.text = string.Empty;
        if (RestartGameEvent!=null)
        RestartGameEvent();      
    }


    private void SetStartSoundVolume()
    {       
        var musicVolume = PlayerPrefs.GetFloat("MusicVolume", 0f); 
        var sfxVolume = PlayerPrefs.GetFloat("SFXVolume", 0f);
        _musicMixer.SetFloat("musicVolume", musicVolume);
        _sfxMixer.SetFloat("sfxVolume", sfxVolume);
        _musicVolumeSlider.value = musicVolume;
        _sfxVolumeSlider.value = sfxVolume;
    }


    private void ActivateOrDiactivateHelpMenu()
    {
        if (_helpMenu.activeInHierarchy)
        {
            _helpMenu.SetActive(false);
            _mainMenu.SetActive(true);
        }
        else {
            _helpMenu.SetActive(true);
            _mainMenu.SetActive(false);
        }
    }
}
