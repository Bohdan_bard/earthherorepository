﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;



public class PlayerController : MonoBehaviour
{
    
    public delegate void LifeChangeAction(int lives);
    public delegate void MassFirePowerUpAction(bool isActive);
    public delegate void ReloadingAction(float delay);

    public static event LifeChangeAction LifeChangeEvent;
    public static event MassFirePowerUpAction MassFireToggleEvent;
    public static event ReloadingAction ReloadEvent;
   

    private const int MassFireCount = 3;
    private const float MassFireAngle = 45f;
    private const int EnemyLayer = 9;
    private const int PowerupLayer = 11;
    private const float MassFireMaxTime = 10f;
   

    [SerializeField]
    private float _speed = default;
  
   
    [SerializeField]
    private GameObject _bullet = null;
    [SerializeField]
    private Transform _firePoint = null;
    [SerializeField]
    private int _lives = 3;
    [SerializeField]
    private GameObject _explosion = null;
    [SerializeField]
    private float _bulletSpeed = default;
    [SerializeField]
    private float _fireDelay = default;
    [SerializeField]
    private AudioSource _shootingAudioSource = null;
    [SerializeField]
    private AudioSource _sirenAudioSource = null;
   
    private Rigidbody2D _rb2d;
    private HitAnimation _hitAnimation;
    private float _timer=0f;
    private bool _massFire=false;
    private float _massFireTimer = 0f;
    private Boundaries _boundaries = null;

    public Boundaries Boundaries
    {     
        set =>  _boundaries = value;      
    }



    private void Awake()
    {
        _rb2d = GetComponent<Rigidbody2D>();
        _hitAnimation = GetComponent<HitAnimation>();      
    }


    private void Start()
    {
        if (LifeChangeEvent != null)
        {
            LifeChangeEvent(_lives);
        }
        if (MassFireToggleEvent != null)
        {
            MassFireToggleEvent(_massFire);
        }
    }


    void Update()
    {
        if (_massFire)
        {
            _massFireTimer += Time.deltaTime;
            if (_massFireTimer>MassFireMaxTime)
            {
                ToggleMassFire();
                _massFireTimer = 0f;
            }
        }

        _timer += Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.Space)&&(_timer>_fireDelay))
        {
            Fire();
            _timer = 0f;
        }

        float horizontalMovement = Input.GetAxis("Horizontal");
        float verticalMovement = Input.GetAxis("Vertical");

        _rb2d.velocity = new Vector2(horizontalMovement * _speed, verticalMovement * _speed);
        _rb2d.position = new Vector2
        (
            Mathf.Clamp(_rb2d.position.x, _boundaries.XMin, _boundaries.XMax),
            Mathf.Clamp(_rb2d.position.y, _boundaries.YMin, _boundaries.YMax)
        );
    }

    private void Fire()
    {
        if (ReloadEvent != null)
            ReloadEvent(_fireDelay);

        _shootingAudioSource.Play();

        if (_massFire)
        {
            for (int i = 0; i < MassFireCount; i++)
            {
                var oneShot = Instantiate(_bullet, _firePoint.position, Quaternion.Euler(new Vector3(0, 0, MassFireAngle-(MassFireAngle*i))));
                oneShot.GetComponent<Rigidbody2D>().velocity = new Vector2(-_bulletSpeed+(_bulletSpeed * i), _bulletSpeed);
            }

            return;
        }

        var shot = Instantiate(_bullet, _firePoint.position, _bullet.transform.rotation);
        shot.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, _bulletSpeed);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == PowerupLayer)
        {
            if (collision.gameObject.GetComponent<PowerUpController>().PowerUpType == PowerUpType.MassFire)
            {
                ToggleMassFire();              
            }
            else
            {
                ChangeLife(1);
            }
            return;
        }

        _sirenAudioSource.Play();
     

        StartCoroutine(_hitAnimation.Hit(GetComponent<SpriteRenderer>()));

        ChangeLife(-1);

        if ((collision.gameObject.layer == EnemyLayer) || (_lives == 0))
        {
            ChangeLife(-_lives);
                   Destroy(gameObject);
            Instantiate(_explosion, transform.position, Quaternion.identity);          
        }       
    }

    private void ToggleMassFire()
    {
        _massFire = (_massFire == true) ? false : true;
        if(MassFireToggleEvent!=null)
        MassFireToggleEvent(_massFire);
    }

    private void ChangeLife(int changeValue)
    {
        _lives+=changeValue;
        if (LifeChangeEvent != null)
        {
            LifeChangeEvent(_lives);
        }
    }
}
