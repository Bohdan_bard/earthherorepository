﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyController : MonoBehaviour
{
  
    public delegate void InstanceDestructionAction(GameObject instance, Vector2 enemyPosition);
   

    
    public static event InstanceDestructionAction EnemyDestroyedEvent;
   
   
    private const int PositiveDirection = 1;
    private const int NegativeDirection = -1;
    private const int PlayerLayer = 10;

  
    [HideInInspector]
    public float LevelModifier;

    
    
    [SerializeField]
    private float _speed = default;
    [SerializeField]
    private float lives = default;
    [SerializeField]
    private GameObject _explosion = null;
    [SerializeField]
    private bool _verticalMovement = default;
    [SerializeField]
    private float _xMoovingOffset = default;
    [SerializeField]
    private float _yMoovingOffset = default;

    private Rigidbody2D _rb2d;
    private int _xDirection = 1;
    private int _yDirection = 1;
    private HitAnimation _hitAnimation;
    private Boundaries _boundaries = null;


    private void Awake()
    {
        _rb2d = GetComponent<Rigidbody2D>();
        _hitAnimation = GetComponent<HitAnimation>();
        _boundaries = new Boundaries(new Vector3(_xMoovingOffset, _yMoovingOffset, 0f));
    }
    

  
    void Update()
    {
        if ((transform.localPosition.x <= _boundaries.XMin)&&(_xDirection== NegativeDirection) || (transform.localPosition.x >= _boundaries.XMax)&&(_xDirection==PositiveDirection))
        {
            _xDirection *=-1;
        }
        if (_verticalMovement)
        {
            if ((transform.localPosition.y <= _boundaries.YMin) && (_yDirection == NegativeDirection) || ((transform.localPosition.y >= _boundaries.YMax) && (_yDirection == PositiveDirection)))
            {
                _yDirection *= -1;
            }
        }
        else
        {
            _yDirection = 0;
        }
        _rb2d.velocity = new Vector2((_speed*LevelModifier)*_xDirection,(_speed*LevelModifier)*_yDirection);
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        StartCoroutine(_hitAnimation.Hit(GetComponent<SpriteRenderer>()));

        if ((--lives <= 0)|| (collision.gameObject.layer== PlayerLayer))
        {
            Instantiate(_explosion, transform.position, transform.rotation);
            if (EnemyDestroyedEvent != null)
            {
                EnemyDestroyedEvent(transform.parent.gameObject, transform.position);
            }
            Destroy(transform.parent.gameObject);
        }
      
    }
 
}
