﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class Boundaries
{
   

   
    private float _xMax = default;
    private float _xMin = default;   
    private float _yMax = default;
    private float _yMin = default;

   public Boundaries(Vector3 maxPoint)
    {
        _xMax = maxPoint.x;
        _xMin = -maxPoint.x;
        _yMax = maxPoint.y;
        _yMin = -maxPoint.y;
    }

    public float XMax => _xMax;
    public float XMin  => _xMin;
    public float YMax => _yMax;
    public float YMin => _yMin;
}

