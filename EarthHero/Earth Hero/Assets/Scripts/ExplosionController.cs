﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionController : MonoBehaviour
{

   
    [SerializeField]
    private AudioSource _explosionSoundSource = null;

   
    void Start()
    {
        _explosionSoundSource.Play();
        Invoke("DestroyGameObject", 3f);
    }

    private void DestroyGameObject()
    {
        Destroy(gameObject);
    }
}
