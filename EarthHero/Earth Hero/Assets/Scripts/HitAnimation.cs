﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitAnimation : MonoBehaviour
{
   private WaitForSeconds waitForSec = new WaitForSeconds(0.1f);

   public IEnumerator Hit(SpriteRenderer sprite)
    {
        Color tempColor = sprite.color;
        for (int i = 0; i < 4; i++)
        {
            tempColor.a = 0.5f;
            sprite.color = tempColor;
            yield return waitForSec;
            tempColor.a = 1f;
            sprite.color = tempColor;
            yield return waitForSec;
        }
    }
}
